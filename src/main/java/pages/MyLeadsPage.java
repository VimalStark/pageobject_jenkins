package pages;

import libraries.Annotations;

public class MyLeadsPage extends Annotations {
	
	public Delete clickCreateLead() {
		driver.findElementByLinkText("Find Leads").click();
		return new Delete();
		
	}

}
